Article premier

Après le titre XIII de la Constitution, il est inséré un titre XIII bis ainsi rédigé :

« TITRE XIII bis :

« DU RÉFÉRENDUM D’INITIATIVE CITOYENNE »

Article 2

Avant le Titre XIV de la Constitution, il est inséré un article 78 ainsi rédigé :

« Art. 78. - Le peuple a droit de proposer les lois, et de les approuver par référendum.

« Un référendum national tendant à l'adoption d’un projet ou d’une proposition de loi se tient sur la demande de toute initiative soutenue par un pourcentage défini des électeurs inscrits sur les listes électorales. Une proposition de loi citoyenne peut être initiée par toute personne et, soutenue par un pourcentage défini des électeurs inscrits sur les listes électorales, celle-ci est soumise à un référendum national.

« Un référendum local tendant à l'adoption de tout projet de délibération ou d'acte relevant de la compétence des collectivités territoriales se tient sur la demande de toute initiative soutenue par un pourcentage défini des électeurs inscrits sur les listes électorales de la circonscription concernée. Une proposition citoyenne de délibération ou d'acte relevant de la compétence des collectivités territoriales peut être initiée par toute personne et, soutenue par un pourcentage défini des électeurs inscrits sur les listes électorales de la circonscription concernée, celle-ci est soumise à un référendum local.

« Les conditions d’application des précédents alinéas sont fixées par une loi organique, les pourcentages sus-mentionnés ne pouvant être supérieurs à deux pour cent, et les référendums devant se tenir dans un délai maximal de trois mois à compter de l’obtention du seuil requis de signatures de soutien. »

Article 3 :

Avant le Titre XIV de la Constitution, il est inséré un article 79 ainsi rédigé :

« Art. 79. - Le peuple a droit d’initiative pour abroger les lois votées en son nom par ses représentants.

« Un référendum national tendant à l'abrogation d’une loi se tient sur la demande de toute
initiative soutenue par un pourcentage défini des électeurs inscrits sur les listes électorales.

« Un référendum local tendant à l'abrogation d’une délibération ou d’un acte relevant de la compétence des collectivités territoriales se tient sur la demande de toute initiative soutenue par un pourcentage défini des électeurs inscrits sur les listes électorales de la circonscription concernée.

« Les conditions d’application des précédents alinéas sont fixées par une loi organique, les pourcentages sus-mentionnés ne pouvant être supérieurs à deux pour cent, et les référendums devant se tenir dans un délai maximal de trois mois à compter de l’obtention du seuil requis de signatures de soutien. »

Article 4

Avant le Titre XIV de la Constitution, il est inséré un article 80 ainsi rédigé :

« Art. 80. - Le peuple a droit de révoquer ses représentants qu’il a élus.

« Le mandat de la Présidente ou du Président de la République est révocable, à l’issue du premier tiers de son mandat, par un référendum national qui se tient sur la demande de toute initiative soutenue par un pourcentage défini des électeurs inscrits sur les listes électorales. Le cas échéant, la révocation est d’effet immédiat, et constitue un des cas d’empêchement définitif prévu par l’article 7.

« Le mandat des parlementaires est révocable par référendum local, à l’issue du premier tiers de leur mandat, convoqué sur la demande de toute initiative soutenue par un pourcentage défini des électeurs inscrits sur les listes électorales de la circonscription concernée.

« Le mandat des élus locaux est révocable par référendum local, à l’issue du premier tiers de leur mandat, convoqué sur la demande de toute initiative soutenue par un pourcentage défini des électeurs inscrits sur les listes électorales de la circonscription concernée.

« Les conditions d’application des précédents alinéas sont fixées par une loi organique, les pourcentages sus-mentionnés ne pouvant être supérieurs à cinq pour cent, et les référendums devant se tenir dans un délai maximal de trois mois à compter de l’obtention du seuil requis de signatures de soutien. »

Article 5

Avant le Titre XIV de la Constitution, il est inséré un article 81 ainsi rédigé :

« Art. 81. - Un peuple a toujours le droit de revoir, de réformer et de changer sa Constitution. Une génération ne peut assujettir à ses lois les générations futures.

« Si au moins cinq pour cent des électeurs inscrits sur les listes électorales en font la demande, un référendum national relatif à la convocation d’une Assemblée constituante se tient, dans les deux mois à compter de l’enregistrement de cette demande.

« Cette Assemblée constituante est composée de représentants du peuple qu’il désigne. Elle est chargée de rédiger et de proposer l’adoption d’une nouvelle constitution. Tout citoyen majeur et détenteur de ses droits civiques et politiques peut y siéger. L’élection de ces représentants aura lieu quatre-vingts jours après la promulgation des résultats du référendum convoquant l’Assemblée constituante.

« La durée maximale des travaux de l’Assemblée constituante est fixée à deux années à compter de sa date d’installation.

« Un référendum sur le résultat des travaux de l’Assemblée constituante est obligatoirement organisé dans les six mois qui suivent la conclusion de ces travaux.

« Une loi organique précise les conditions d’application du présent article. »
